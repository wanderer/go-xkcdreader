# [go-xkcdreader](https://git.dotya.ml/wanderer/go-xkcdreader)

> an offline-capable xkcd webcomic reader written in Go

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)
[![Cachix](https://img.shields.io/badge/cachix-go--xkcdreader-blue.svg)](https://go-xkcdreader.cachix.org)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/wanderer/go-xkcdreader/status.svg?ref=refs/heads/development)](https://drone.dotya.ml/wanderer/go-xkcdreader)
[![Go Report Card](https://goreportcard.com/badge/git.dotya.ml/wanderer/go-xkcdreader)](https://goreportcard.com/report/git.dotya.ml/wanderer/go-xkcdreader)
[![Go Documentation](https://godocs.io/git.dotya.ml/wanderer/go-xkcdreader?status.svg)](https://godocs.io/git.dotya.ml/wanderer/go-xkcdreader)

### disclaimer
WIP :construction:

### ad nix flake ❄
this project is provided as a **nix flake** for both convenience and
reproducibility.  
and also because I wanted to play with `nix` and its flakes just a wee bit.

> *note on the nix-related steps*: make sure you have
> [`nix`](https://nixos.org/learn.html) installed and optionally the flake
> functionality enabled, the Go toolchain and a minimal set of Go development
> goodies will get pulled in automatically. you might want to speed up the
> builds by making use of project's build cache: `cachix use go-xkcdreader`.

that means you can get started:
### developing using nix (flakes enabled)
run `nix develop` in the project folder *first*, then `go {run,build,fmt,...}`
about your day.

### developing using nix (without flakes)
> for compatibility with non-flake-enabled systems, this project uses
> [`github:edolstra/flake-compat`](https://github.com/edolstra/flake-compat).

run `nix-shell` in the project folder *first*, then `go {run,build,fmt,...}`
about your day.

### developing without nix
> *note*: you only need to have the Go toolchain and [Fyne
> dependencies](https://developer.fyne.io/started/#prerequisites) installed for
> this.

if you don't want to use the flake provided or don't have `nix` installed, you
can use the standard `go` tool:
run `go {run,build,fmt,...}` in the project folder.


### COPYING
Licensed under GPL-3.0-or-later (see [LICENSE](LICENSE) for details).
