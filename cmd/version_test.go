// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cmd

import (
	"bytes"
	"errors"
	"os"
	"regexp"
	"testing"
)

func TestNixFlake_GosrcVersionStringEquivalence(t *testing.T) {
	t.Parallel()

	want := GetShortVersion()

	fData, err := os.ReadFile("../flake.nix")
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			t.Errorf("Failed to open flake.nix: %q", err)
		}

		t.Errorf("Error reading flake: %q", err)
	}

	got := bytes.Contains(fData, []byte("      version = \""+want+"\""))

	if !got {
		t.Errorf("Version string '%q' not found in ../flake.nix", want)
	}
}

func TestVersionStringFormat(t *testing.T) {
	t.Parallel()

	v := version
	// this expression matches the format of "vX.Y.Z" where {X,Y,Z} are digits
	// (such as 0 or 123)
	re := regexp.MustCompile(`^(v\d+\.\d+\.\d+)$`)
	match := re.MatchString(v)

	if !match {
		t.Errorf("Incorrect version string format, must match regex: '%q', provided version string: '%q'", re.String(), v)
	}
}

func TestGetVersion(t *testing.T) {
	want := appName + " - " + version
	got := getVersion()

	if want != got {
		t.Errorf("Incorrect version string, want: '%q', got: '%q'", want, got)
	}
}

// set shortVersion variable manually.
func TestGetVersionWithShortVersionVar(t *testing.T) {
	shortVersion = true
	want := version

	got := getVersion()

	if want != got {
		t.Errorf("Incorrect version string when 'shortVersion' is true, want: '%q', got: '%q'", want, got)
	}
}

// explicitly get short version.
func TestGetShortVersion(t *testing.T) {
	want := version
	got := GetShortVersion()

	if want != got {
		t.Errorf("Incorrect 'short' version string, want: '%q', got: '%q'", want, got)
	}
}
