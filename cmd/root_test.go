// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cmd

import (
	"testing"
)

func TestGetAppName(t *testing.T) {
	want := "go-xkcdreader"
	got := GetAppName()

	if got != want {
		t.Errorf("App name strings don't match, want: %q', got: %q ", want, got)
	}
}

func TestExecuteRootCmd(t *testing.T) {
	rootcmd := *Root

	err := rootcmd.Execute()
	if err != nil {
		t.Error("Failed to execute the root cmd")
	}
}

// is this valid? dunno...
func TestExecuteFunc(t *testing.T) {
	Execute()
}
