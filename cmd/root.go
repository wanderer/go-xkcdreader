// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
)

// appName is the name of the app.
const appName = "go-xkcdreader"

// Root is the main go-xkcdreader command.
var Root = &cobra.Command{
	Use:   appName,
	Short: "an offline-capable xkcd webcomic reader written in Go",
	RunE: func(cmd *cobra.Command, args []string) error {
		// the app will be started from here
		// a noop atm
		return nil
	},
}

// GetAppName returns the name of the application.
func GetAppName() string {
	return appName
}

// Execute is the entrypoint of cobra's poison.
func Execute() {
	if err := Root.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// help redefines the original help func, essentially adding a way to exit the
// application properly after showing help message.
func help(*cobra.Command, []string) {
	err := Root.Usage()
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	os.Exit(0)
}

func init() {
	Root.SetHelpFunc(help)
}
