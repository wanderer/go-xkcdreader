// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// version holds app version string.
var version = "v0.0.13"

// used to determine whether to print short or long version string.
var shortVersion = false

var cmdVersion = &cobra.Command{
	Use:   "version",
	Short: "Print version information and exit",
	Long:  `All software has versions. This is ` + appName + `'s`,
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Fprintln(os.Stderr, getVersion())
		return nil
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		// exit with code 0 after showing the version
		os.Exit(0)
	},
}

// get (short if -s/--short flag is supplied) version.
func getVersion() string {
	if !shortVersion {
		return appName + " - " + version
	}

	return GetShortVersion()
}

// GetShortVersion returns a bare version string.
func GetShortVersion() string {
	return version
}

// the init func registers the commands and flags with cobra.
func init() {
	cmdVersion.Flags().BoolVarP(&shortVersion, "short", "s", false, "print just the version string and nothing else")
	Root.AddCommand(cmdVersion)
}
