// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"testing"
)

// does this test even make sense?
func TestExecMain(t *testing.T) {
	go main()
	t.Log("Main executed successfully")
}
