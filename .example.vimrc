augroup vimgo_local
	au!

	function! Runhidden()
		cd `git rev-parse --show-toplevel`
		echo '...running go-xkcdreader'
		term ++hidden ++open nixGL go run -tags wayland -v .
    endfunction

	au FileType go nmap <leader>gr :exec Runhidden()<cr>
	au FileType go nmap <leader>r :exec Runhidden()<cr>
	au FileType go nmap <leader>gc <Plug>(go-coverage-toggle)

	" if using vim-go, this sets gofumpt as the main formatter
	let g:go_gopls_gofumpt=1
	let g:go_fmt_command='gofumpt'
augroup END
