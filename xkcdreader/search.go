// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package xkcdreader

import (
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func makeSearchTab() *fyne.Container {
	c := container.NewMax(
		container.NewVBox(
			widget.NewLabel("You want me to look in..."),
			container.NewHBox(
				widget.NewCheck("xkcd", func(value bool) {
					// TODO(me): have this preference saved in a config or sth
					log.Println("'search in xkcd' set to", value)
				}),
				widget.NewCheck("what if?", func(value bool) {
					log.Println("'search in what if?' set to", value)
				})),
			makeSearchEntry(),
		),
	)

	return c
}

func makeSearchEntry() *widget.Entry {
	s := widget.NewEntry()
	s.SetPlaceHolder("Search phrase, comic number...")
	s.Resize(fyne.Size{Height: 100})

	return s
}
