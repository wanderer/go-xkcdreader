// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package xkcd

import (
	"math"
	"testing"

	"gitea.com/jolheiser/xkcd"
)

func TestGetComic(t *testing.T) {
	comicNum := 2625
	comic := GetComic(comicNum)

	comic2625 := xkcd.Comic{
		Month:      "5",
		Num:        2625,
		Link:       "",
		Year:       "2022",
		News:       "",
		SafeTitle:  "Field Topology",
		Transcript: "",
		Alt:        "The combination croquet set/10-lane pool can also be used for some varieties of foosball and Skee-Ball.",
		Img:        "https://imgs.xkcd.com/comics/field_topology.png",
		Title:      "Field Topology",
		Day:        "27",
	}

	if *comic != comic2625 {
		t.Log("Comic does not match test data")
		t.FailNow()
	}
}

func TestGetComic_Bad(t *testing.T) {
	comicNum := math.MaxInt32
	comic := GetComic(comicNum)

	if comic != nil {
		t.Logf("No comic should have been returned for comicNum: %d", comicNum)
		t.FailNow()
	}
}

func TestGetLatest_Bad(t *testing.T) {
	comic := GetLatest()

	if comic == nil {
		t.Log("Could not get latest comic")
		t.FailNow()
	}
}
