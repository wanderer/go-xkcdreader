// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package xkcd

import (
	"context"
	"log"

	"gitea.com/jolheiser/xkcd"
)

var xkcdClient = xkcd.New()

// GetLatest grabs the latest available xkcd comic.
func GetLatest() *xkcd.Comic {
	comic, err := xkcdClient.Current(context.Background())
	if err != nil {
		// TODO(me): handle this
		log.Println(err)
	}

	return comic
}

// GetComic grabs xkcd comic no. "num", as provided by the caller.
func GetComic(num int) *xkcd.Comic {
	comic, err := xkcdClient.Comic(context.Background(), num)
	if err != nil {
		// TODO(me): handle this
		log.Println(err)

		comic = nil
	}

	return comic
}
