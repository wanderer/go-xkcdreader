// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package xkcdreader

import (
	"testing"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"git.dotya.ml/wanderer/go-xkcdreader/cmd"
)

func TestGreetingText(t *testing.T) {
	want := "welcome to " + cmd.GetAppName()
	got := makeGreeting()

	if got.Text != want {
		t.Errorf("Incorrect initial greeting, want: %q, got: %q", want, got.Text)
	}
}

func TestToolbar(t *testing.T) {
	wantItems := 4
	gotToolbar := makeToolbar()

	if len(gotToolbar.Items) != wantItems {
		t.Errorf("Incorrect number of toolbar items, want: %d, got: %d", wantItems, len(gotToolbar.Items))
	}
}

func TestTabs(t *testing.T) {
	// wantTabsNum := 2
	// gotTabs := makeTabs()
	//
	// if len(gotTabs.Items) != wantTabsNum {
	// t.Errorf("Incorrect number of app tabs, want: %d, got: %d", wantTabsNum, len(gotTabs.Items))
	// }
	//
	// tab0 := 0
	// gotTabs.SelectIndex(tab0)
	//
	// if gotTabs.SelectedIndex() != tab0 {
	// t.Errorf("Failed to select tab %d", tab0)
	// }
	//
	// tab0Text := "xkcd comic"
	// if gotTabs.CurrentTab().Text != tab0Text {
	// t.Errorf("Tab0 text is not %q", tab0Text)
	// }

	//
	// hitting the following test failure with the above:
	//
	/*
		--- FAIL: TestTabs (0.00s)
		panic: runtime error: invalid memory address or nil pointer dereference [recovered]
			panic: runtime error: invalid memory address or nil pointer dereference
		[signal SIGSEGV: segmentation violation code=0x1 addr=0x68 pc=0x55e29ca4fbb1]

		goroutine 35 [running]:
		testing.tRunner.func1.2({0x55e29cda1240, 0x55e29d06a1e0})
			testing/testing.go:1389 +0x24e
		testing.tRunner.func1()
			testing/testing.go:1392 +0x39f
		panic({0x55e29cda1240, 0x55e29d06a1e0})
			runtime/panic.go:838 +0x207
		fyne.io/fyne/v2/theme.(*builtinTheme).Color(0x60?, {0x55e29cc346ea?, 0xc000120d00?}, 0x55e29ca50699?)
			fyne.io/fyne/v2@v2.1.4/theme/theme.go:322 +0x71
		fyne.io/fyne/v2/theme.safeColorLookup({0x55e29cc346ea, 0x7}, 0x1?)
			fyne.io/fyne/v2@v2.1.4/theme/theme.go:644 +0x47
		fyne.io/fyne/v2/theme.PrimaryColor()
			fyne.io/fyne/v2@v2.1.4/theme/theme.go:449 +0x2d
		fyne.io/fyne/v2/container.(*AppTabs).CreateRenderer(0xc00014a000)
			fyne.io/fyne/v2@v2.1.4/container/apptabs.go:55 +0x85
		fyne.io/fyne/v2/internal/cache.Renderer({0x55e29ce17918, 0xc00014a000})
			fyne.io/fyne/v2@v2.1.4/internal/cache/widget.go:33 +0x162
		fyne.io/fyne/v2/widget.(*BaseWidget).Refresh(0x55e29ce171f8?)
			fyne.io/fyne/v2@v2.1.4/widget/widget.go:137 +0x25
		fyne.io/fyne/v2/container.(*AppTabs).SetItems(0xc00014a000, {0xc000130080?, 0xc00014a000?, 0x55e29cd837e0?})
			fyne.io/fyne/v2@v2.1.4/container/apptabs.go:182 +0x45
		fyne.io/fyne/v2/container.NewAppTabs({0xc000130080, 0x2, 0x2})
			fyne.io/fyne/v2@v2.1.4/container/apptabs.go:42 +0x5e
		git.dotya.ml/wanderer/go-xkcdreader/xkcdreader.makeTabs()
			git.dotya.ml/wanderer/go-xkcdreader/xkcdreader/app.go:57 +0x145
		git.dotya.ml/wanderer/go-xkcdreader/xkcdreader.TestTabs(0xc000102820)
			git.dotya.ml/wanderer/go-xkcdreader/xkcdreader/app_test.go:32 +0x25
		testing.tRunner(0xc000102820, 0x55e29ce0f890)
			testing/testing.go:1439 +0x102
		created by testing.(*T).Run
			testing/testing.go:1486 +0x35f
		FAIL	git.dotya.ml/wanderer/go-xkcdreader/xkcdreader	0.011s
		FAIL
	*/
}

func TestNewApp(t *testing.T) {
	// init the application
	newApp()

	// get the application from var a
	testA := a

	if testA == nil {
		t.Error("Failed to init application")
	}
}

func TestGetApp(t *testing.T) {
	// init the application
	newApp()

	got := getApp()

	if got == nil {
		t.Error("Failed to get application pointer using getApp()")
	}
}

func TestGetImgMinSize(t *testing.T) {
	// init
	newApp()

	// this is relative to the test file
	imgPath := "assets/comic.png"
	dimens := struct {
		h float32
		w float32
	}{
		h: 383.0,
		w: 273.0,
	}

	got := getImg(imgPath, true)
	got.SetMinSize(
		fyne.Size{
			Height: 383.0,
			Width:  273.0,
		},
	)

	if got.MinSize().Height != dimens.h {
		t.Errorf("Failed to get img w/ proper height, want: %f, got: %f", dimens.h, got.Size().Height)
	}

	if got.MinSize().Width != dimens.w {
		t.Errorf("Failed to get img w/ proper width, want: %f, got: %f", dimens.w, got.Size().Width)
	}
}

func TestGetImg(t *testing.T) {
	// init
	newApp()

	// this is relative to the test file
	imgPath := "assets/comic.png"

	got := getImg(imgPath, true)
	got.SetMinSize(
		fyne.Size{
			Height: 383.0,
			Width:  273.0,
		},
	)

	testImg := canvas.NewImageFromFile(imgPath)

	gotA := got.Image
	testA := testImg.Image

	if gotA != nil && testA != nil {
		t.Fatal("test img or fetched img are nil")
	}

	if gotA != testA {
		t.Fatal("Images differ")
	}
}

func TestGetComicLatest(t *testing.T) {
	want := latestComic

	// attempt comic refresh
	getComicLatest()

	got := latestComic

	if want.Img != got.Img ||
		want.Alt != got.Alt ||
		want.Day != got.Day ||
		want.Link != got.Link ||
		want.Month != got.Month ||
		want.News != got.News ||
		want.SafeTitle != got.SafeTitle ||
		want.Title != got.Title ||
		want.Transcript != got.Transcript ||
		want.Year != got.Year {
		t.Error("latestComic contains stale comic, comic data differ")
	}
}

func TestGetComicLatest_MinSize(t *testing.T) {
	want := struct {
		height float32
		width  float32
	}{
		height: 650.0,
		width:  750.0,
	}

	getComicLatest()

	got := getImg(latestComic.Img, false)

	if want.height != got.MinSize().Height {
		t.Errorf("Latest comic height differs, want: %f, got: %f",
			want.height, got.MinSize().Height)
	}

	if want.width != got.MinSize().Width {
		t.Errorf("Latest comic width differs, want: %f, got: %f",
			want.width, got.MinSize().Width)
	}
}
