// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package xkcdreader

import (
	// for embedding standard license header.
	_ "embed"
	"log"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"git.dotya.ml/wanderer/go-xkcdreader/cmd"
)

type project struct {
	name         string
	version      string
	homepage     string
	codeHomepage string
	author       string
	license      string
}

var (
	authorInfo = "Adam Mirre <a_mirre at utb dot cz>"
	projectURL = "https://git.dotya.ml/wanderer/go-xkcdreader"
)

//go:embed assets/standard-license-header.txt
var l string

func aboutWindow() {
	app := getApp()
	w := app.NewWindow("about - " + cmd.GetAppName())

	projectInfo := &project{
		name:         cmd.GetAppName(),
		homepage:     projectURL,
		codeHomepage: projectURL,
		version:      cmd.GetShortVersion(),
		author:       authorInfo,
		license:      "GPL-3.0-or-later",
	}
	aboutTitle := container.New(
		layout.NewHBoxLayout(),
		&layout.Spacer{},
		&widget.Label{Text: "About", TextStyle: fyne.TextStyle{Bold: true}},
		&layout.Spacer{},
	)
	licenseScroll := container.NewScroll(
		&widget.Label{Text: l, TextStyle: fyne.TextStyle{Monospace: true}},
	)
	licenseScroll.SetMinSize(fyne.Size{Height: 200, Width: 300})

	ac := widget.NewAccordion(widget.NewAccordionItem(
		projectInfo.license,
		licenseScroll,
	))

	homepageScroll := container.NewHScroll(
		&widget.Hyperlink{Text: projectInfo.codeHomepage, TextStyle: fyne.TextStyle{Monospace: true}},
	)
	homepageScroll.SetMinSize(fyne.Size{Width: 300})

	about := container.New(
		layout.NewFormLayout(),
		&widget.Label{Text: "Name and version"},
		&widget.Label{Text: projectInfo.name + " - " + projectInfo.version, TextStyle: fyne.TextStyle{Bold: true}},
		&widget.Label{Text: "Author"},
		&widget.Label{Text: projectInfo.author, TextStyle: fyne.TextStyle{Bold: true}},
		&widget.Label{Text: "Project homepage"},
		homepageScroll,
		&widget.Label{Text: "License"},
		ac,
	)

	w.SetContent(container.NewVBox(
		aboutTitle,
		container.NewHBox(
			&layout.Spacer{},
			about,
			&layout.Spacer{},
		),
	))

	w.Resize(fyne.NewSize(600, 420))

	log.Println("Show About window")

	w.Show()
}
