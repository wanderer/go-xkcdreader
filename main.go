// Copyright 2022 wanderer <a_mirre at utb dot cz>
// SPDX-License-Identifier: GPL-3.0-or-later

package main

import (
	"log"

	"git.dotya.ml/wanderer/go-xkcdreader/cmd"
	"git.dotya.ml/wanderer/go-xkcdreader/xkcdreader"
)

var version = cmd.GetShortVersion()

func main() {
	cmd.Execute()

	log.Println("Starting " + cmd.GetAppName() + " " + version)

	xkcdreader.RunApp()

	log.Println("Exited")
}
