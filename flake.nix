{
  description = "an offline-capable xkcd webcomic reader written in Go";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nixgl.url = "github:guibou/nixGL";
    nixgl.inputs.nixpkgs.follows = "nixpkgs";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-filter = {
      url = "github:numtide/nix-filter";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixgl, nix-filter, nixpkgs, ... }:
    let

      projname = "go-xkcdreader";

      # to work with older version of flakes
      lastModifiedDate =
        self.lastModifiedDate or self.lastModified or "19700101";

      # Generate a user-friendly version number.
      # version = builtins.substring 0 8 lastModifiedDate;
      version = "v0.0.13";

      # System types to support.
      supportedSystems =
        [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" ];

      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = forAllSystems (system: import nixpkgs {
        inherit system;
        overlays = [
          (import ./overlay.nix)
        ];
      });
    in
    rec {
      formatter = forAllSystems (system:
        nixpkgsFor.${system}.nixpkgs-fmt
      );

      packages = forAllSystems (system:
        let
          pkgs = nixpkgsFor.${system};
          inherit (pkgs) lib overlays;# -> lib = pkgs.lib;overlays = pkgs.overlays;
        in
        rec {
          go-xkcdreader = with pkgs; buildGoModule rec {
            pname = "${projname}";
            buildInputs = [
              gcc
              libglvnd # instead of libGL
              wayland
              libxkbcommon
              xorg.libX11
              xorg.libXcursor
              xorg.libXfixes
              xorg.libXrandr
              xorg.libXrender
              xorg.xrandr
              xorg.libXinerama
              xorg.xinput
              xorg.libXi
              xorg.libXxf86vm
              # xorg.xorgserver
            ];
            nativeBuildInputs = [ pkgconfig ];

            overrideModAttrs = _: {
              # GOPROXY = "direct";
            };

            inherit version;
            doCheck = false;
            # use go.mod for managing go deps, instead of vendor-only dir
            proxyVendor = true;
            tags = [ "wayland" ]; # go "-tags" to build with
            ldflags = [
              "-s"
              "-w"
              "-X cmd.version=${version}"
            ];

            modSha256 = lib.fakeSha256;
            # dont't forget to update vendorSha256 whenever go.mod or go.sum change
            vendorSha256 = "sha256-LvdcTbj8cFlaIBsq1VLfLF2Tu9HiZzGO8igD766nMLE=";

            # In 'nix develop', we don't need a copy of the source tree
            # in the Nix store.
            src = nix-filter.lib.filter {
              root = lib.cleanSource ./.;
              exclude = [
                ./flake.nix
                ./flake.lock
                ./default.nix
                ./shell.nix

                ./check-fmt
                ./README.md
                ./LICENSE

                ./.drone.starlark
                ./.envrc
                ./.example.vimrc
                ./.gitattributes
                ./.gitignore
                ./.golangci.yml
                ./.pre-commit-config.yaml
              ];
            };

            meta = {
              description = "an offline-capable xkcd webcomic reader written in Go";
              homepage = "https://git.dotya.ml/wanderer/${projname}";
              license = lib.licenses.gpl3;
              maintainers = [ "wanderer" ];
              platforms = lib.platforms.linux ++ lib.platforms.darwin;
            };
          };
          default = go-xkcdreader;
        });


      apps = forAllSystems (system: rec {
        go-xkcdreader = {
          type = "app";
          program =
            "${self.packages.${system}.${projname}}/bin/${projname}";
        };
        default = go-xkcdreader;
      });

      devShells = forAllSystems (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ nixgl.overlay ];
          };
          goPkgs = import nixpkgs {
            useFetched = true;
            inherit system;
            overlays = self.overlays or [ ] ++ [
              (import ./overlay.nix)
            ];
          };
          gob = pkgs.writeShellScriptBin "gob" ''
            cd $(git rev-parse --show-toplevel)
            go build -v ./...
          '';
          gota = pkgs.writeShellScriptBin "gota" ''
            cd $(git rev-parse --show-toplevel)
            go test ./...
          '';
          upcache = pkgs.writeShellScriptBin "upcache" ''
            ## refs:
            ##   https://fzakaria.com/2020/08/11/caching-your-nix-shell.html
            ##   https://nixos.wiki/wiki/Caching_nix_shell_build_inputs
            nix-store --query --references $(nix-instantiate shell.nix) | \
              xargs nix-store --realise | \
              xargs nix-store --query --requisites | \
              cachix push ${projname}
          '';
          add-license = pkgs.writeShellScriptBin "add-license" ''
            go run github.com/google/addlicense@v1.0.0 -v \
              -c "wanderer <a_mirre at utb dot cz>" \
              -l "GPL-3.0-or-later" -s .
          '';
        in
        {
          default = with pkgs; mkShell
            {
              name = "${projname}-" + version;

              GOFLAGS = "-buildmode=pie -trimpath -mod=readonly -modcacherw";
              GOLDFLAGS = "-s -w -X cmd.version=${version}";
              CGO_CFLAGS = "-g2 -Og -mtune=generic";
              CGO_LDFLAGS = "-Wl,-O1,-sort-common,-as-needed,-z,relro,-z,now,-flto -pthread";
              GOPROXY = "direct";

              shellHook = ''
                echo " -- in go-xkcdreader shell..."
              '';

              nativeBuildInputs = [
                ## uncomment the line below if using an Intel GPU when
                ## developing
                # pkgs.nixgl.nixGLIntel

                ## this won't work without passing --impure, doesn't care what
                ## GPU vendor you use, it figures it out automatically
                # pkgs.nixgl.auto.nixGLDefault # requires --impure
              ];
              packages = [
                ## use patched Go, since it's supposed to be faster
                goPkgs.go
                ## if you wish to use this, uncomment the related block in
                ## overlay.nix and the next line
                # goPkgs.dominikh.go-tools
                # goPkgs.patchelf-x86_64
                # goPkgs.patchelf-aarch64

                ## ad-hoc cmds
                gob
                gota
                upcache
                add-license

                gopls
                gofumpt
                go-tools
                golangci-lint
                nixpkgs-fmt
              ] ++ (if stdenv.isLinux then [

                pkgconfig

                # fyne deps
                libglvnd # instead of libGL
                wayland
                libxkbcommon
                xorg.libX11
                xorg.libXcursor
                xorg.libXfixes
                xorg.libXrandr
                xorg.libXrender
                xorg.xrandr
                xorg.libXinerama
                xorg.xinput
                xorg.libXi
                xorg.libXxf86vm
                xorg.xorgserver

              ] else [ ]);
            };
        }
      );
    };
}
